/* START: función main */
function main() {


    $("#button_simular").html('<i class="fas fa-spinner fa-pulse"></i>');
    var delayInMilliseconds = 1090; //1 second

    setTimeout(function () {
        var start_time = Date.now();
        var validation = true;
        if ($("#flujo_solvente").val() == "") {
            if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
            validation = false;
        }
        if ($("#temperatura_solvente").val() == "") {
            if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
            validation = false;
        }
        if ($("#presion_solvente").val() == "") {
            if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
            validation = false;
        }
        if ($("#flujo_gas").val() == "") {
            if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
            validation = false;
        }
        if ($("#temperatura_gas").val() == "") {
            if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
            validation = false;
        }
        if ($("#presion_gas").val() == "") {
            if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
            validation = false;
        }

        /* START: Declaración de variables dadas por el usuario */

        /* Componentes */

        var input_composition = [];
        if ($("#pentano_checkbox").is(':checked') == true) {
            if ($("#pentano_input").val() == "") {
                if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
                validation = false;
            }
            var pentano = parseFloat($("#pentano_input").val()).toFixed(4);
            input_composition.push([
                'Pentano', pentano, "#C2185B"
            ]);
        }
        if ($("#butano_checkbox").is(':checked') == true) {
            if ($("#butano_input").val() == "") {
                if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
                validation = false;
            }
            var butano = parseFloat($("#butano_input").val()).toFixed(4);
            input_composition.push([
                'Butano', butano, "#1976D2"
            ]);
        }
        if ($("#propano_checkbox").is(':checked') == true) {
            if ($("#propano_input").val() == "") {
                if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
                validation = false;
            }
            var propano = parseFloat($("#propano_input").val()).toFixed(4);
            input_composition.push([
                'Propano', propano, "#0097A7"
            ]);
        }
        if ($("#sulfuro_de_hidrogeno_checkbox").is(':checked') == true) {
            if ($("#sulfuro_de_hidrogeno_input").val() == "") {
                if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
                validation = false;
            }
            var sulfuro_de_hidrogeno = parseFloat($("#sulfuro_de_hidrogeno_input").val()).toFixed(4);
            input_composition.push([
                'Sulfuro_de_hidrogeno', sulfuro_de_hidrogeno, "#512DA8"
            ]);
        }

        if ($("#etano_checkbox").is(':checked') == true) {
            if ($("#etano_input").val() == "") {
                if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
                validation = false;
            }
            var etano = parseFloat($("#etano_input").val()).toFixed(4);
            input_composition.push([
                'Etano', etano, "#FBC02D"
            ]);
        }

        if ($("#dioxido_de_carbono_checkbox").is(':checked') == true) {
            if ($("#dioxido_de_carbono_input").val() == "") {
                if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
                validation = false;
            }
            var dioxido_de_carbono = parseFloat($("#dioxido_de_carbono_input").val()).toFixed(4);
            input_composition.push([
                'Dioxido_de_carbono', dioxido_de_carbono, "#388E3C"
            ]);
        }

        if ($("#metano_checkbox").is(':checked') == true) {
            if ($("#metano_input").val() == "") {
                if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
                validation = false;
            }
            var metano = parseFloat($("#metano_input").val()).toFixed(4);
            input_composition.push([
                'Metano', metano, "#F57C00"
            ]);
        }

        if ($("#propileno_checkbox").is(':checked') == true) {
            if ($("#propileno_input").val() == "") {
                if (validation == true) toastr.error('Please, check all the fields. ', 'Error!');
                validation = false;
            }
            var propileno = parseFloat($("#propileno_input").val()).toFixed(4);
            input_composition.push([
                'Propileno', propileno, "#E64A19"
            ]);
        }
        if ($("#metilciclopentano_radio").is(':checked') == true) {
            input_composition.push([
                'metilciclopentano', 1.0, "#5D4037"
            ]);
        }

        if ($("#estireno_radio").is(':checked') == true) {
            input_composition.push([
                'estireno', 1.0, "#5D4037"
            ]);
        }
        if ($("#metilnaftaleno_radio").is(':checked') == true) {
            input_composition.push([
                'metilnaftaleno', 1.0, "#5D4037"
            ]);
        }
        if ($("#ciclohexano_radio").is(':checked') == true) {
            input_composition.push([
                'ciclohexano', 1.0, "#5D4037"
            ]);
        }

        var sum_composition = 0;
        for (i = 0; i < input_composition.length; i++) {
            sum_composition += parseFloat(input_composition[i][1]);
        }
        console.log("sum_composition");
        console.log(sum_composition);

        if (sum_composition != 2) {
            validation = false;
            toastr.error('Composition not valid.', 'Error!');
        }

        
    if (validation == true) {
        console.log("Componenetes de entrada: ")
        console.log(input_composition);

        /* Liquidos (Solventes)*/
        var liquid_flow = parseFloat($("#flujo_solvente").val());
        var liquid_temperature = parseFloat($("#temperatura_solvente").val());
        var liquid_pressure = parseFloat($("#presion_solvente").val());


        /* Gases */
        var gas_flow = parseFloat($("#flujo_gas").val());
        var gas_temperature = parseFloat($("#temperatura_gas").val());
        var gas_pressure = parseFloat($("#presion_gas").val());

        /* Torre */
        var pressure_dome = parseFloat($("#presion_torre_inicial").val());
        var pressure_drop = parseFloat($("#presion_torre_final").val());
        /* END: Declaración de variables dadas por el usuario */

        /* START: Declaración de constantes */
        const stage = parseInt($("#numero_etapas").val());
        // Order: Tci, Pci, Wi, aj0, aj1, aj2, aj3, aj4
        const db_component = [
            ['metano', 343.08, 667.2124352, 0.012, 0.8245223, 0.03806383, 0.000008864745, -0.00000000461153, 0.000000000001822959],
            ['etano', 549.54, 706.8186528, 0.1, 11.51606, 0.0140309, 0.00000085403, -0.0000000106078, 0.000000000003162199],
            ['propano', 665.64, 616.2901554, 0.152, 15.68683, 0.02504953, 0.00001404258, -0.0000000352626, 0.00000000001864467],
            ['butano', 765.18, 550.7150259, 0.2, 20.79783, 0.03143287, 0.00001929511, -0.00000004538652, 0.00000000002380972],
            ['pentano', 845.46, 488.9119171, 0.252, 25.64627, 0.0389176, 0.00002397294, -0.00000005842615, 0.0000000003079918],
            ['sulfuro_de_hidrogeno', 672.3, 1300.331606, 0.094, 8.031194, 0.0009868632, 0.000002388543, 0.00000000159311, 0.000000000000320326],
            ['dioxido_de_carbono', 547.56, 1071.108808, 0.224, 8.348605, -0.006475766, -0.000003555025, 0.000000001194595, -0.0000000000001851702],
            ['propileno', 658.026, 671.5247, 0.137, 13.63267, 0.02106998, 0.00000249845, -0.00000001146863, 0.000000000005247386],
            ['ciclohexano', 996.44, 548.2426, 0.212, 21.00016, 0.05627391, 0.00001129438, -0.00000003606168, 0.00000000001482606],
            ['metilciclopentano', 959.022, 548.2426, 0.230, 22.02735, 0.05465972, 0.000005935, 0.00000003442294, 0.0000000000164543],
            ['estireno', 1144.8, 554.0442, 0.295, 24.82866, 0.05843, -0.000025693, 0.000000003432486, 0.0000000000008297016],
            ['metilnaftaleno', 1389.6, 578.7006, 0.296, 29.351, 0.0747558, -0.00001560299, -0.00000001428024, 0.000000000007089441],
        ];

        /* END: Declaración de constantes */
        var Qj = [];
        for (j = 0; j < stage; j++) {
            Qj.push(parseFloat($("#etapa_" + (j + 1)).val()));
        }
        console.log("Qj");
        console.log(Qj);
        /* START: Busqueda de datos en db_componenet */
        const Tci = [];
        const Pci = [];
        const Wi = [];
        const aj0 = [];
        const aj1 = [];
        const aj2 = [];
        const aj3 = [];
        const aj4 = [];
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < db_component.length; j++) {
                if (input_composition[i][0] == db_component[j][0]) {
                    Tci.push(db_component[j][1]);
                    Pci.push(db_component[j][2]);
                    Wi.push(db_component[j][3]);
                    aj0.push(db_component[j][4]);
                    aj1.push(db_component[j][5]);
                    aj2.push(db_component[j][6]);
                    aj3.push(db_component[j][7]);
                    aj4.push(db_component[j][8]);
                }
            }
        }


        /* START: Calculo de tjInit */
        var TjInit = [];
        for (j = 0; j < stage; j++) {
            if (j == 0) {
                TjInit[j] = liquid_temperature;
            } else if (j == (stage - 1)) {
                TjInit[j] = gas_temperature;
            } else {
                TjInit[j] = TjInit[j - 1] + ((gas_temperature - liquid_temperature) / (stage - 1));
            }
        }
        console.log("TjInit:");
        console.log(TjInit);
        /* END: Calculo de tjInit */

        /* START: Calculo de PjInit */
        var PjInit = [];
        for (j = 0; j < stage; j++) {
            if (j == 0) {
                PjInit[j] = pressure_dome;
            }
            else {
                PjInit[j] = PjInit[j - 1] + pressure_drop;
            }
        }
        console.log("PjInit:");
        console.log(PjInit);
        /* END: Calculo de PjInit */
        /* START: Calculo de mi y aij */
        var mi = [];
        var aij = createMatrix();

        for (i = 0; i < input_composition.length; i++) {
            mi[i] = 0.480 + 1.574 * Wi[i] - 0.176 * Math.pow(Wi[i], 2);
        }

        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                aij[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((TjInit[j] / Tci[i]), 0.5)), 2);
            }
        }
        console.log("mi:");
        console.log(mi);
        console.log("aij:");
        console.log(aij);



        /* START Realiza ecuación Aij y Bij*/
        var Aij = createMatrix();
        var Bij = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                Aij[i][j] = (0.42747 * aij[i][j]) * ((PjInit[j] / Pci[i]) / (Math.pow(TjInit[j] / Tci[i], 2)));
                Bij[i][j] = 0.08664 * ((PjInit[j] / Pci[i]) / (TjInit[j] / Tci[i]));
            }
        }
        console.log("Aij:");
        console.log(Aij);
        console.log("Bij:");
        console.log(Bij);
        /* END Realiza ecuación Aij y Bij */

        /* START: Obtiene de mayor prioridad */
        var yInit = createMatrix();
        /* STRAT : Calculo composicion  y1 */

        yInit[0][0] = 0;
        yInit[0][(stage - 1)] = input_composition[0][1] - (input_composition[0][1] / (stage));
        for (j = (stage - 2); j > 0; j--) {
            yInit[0][j] = yInit[0][j + 1] - (input_composition[0][1] / stage);
        }

        var VjInit = [];
        for (j = 0; j < stage; j++) {
            VjInit[j] = ((1 - input_composition[0][1]) * gas_flow) / (1 - yInit[0][j]);
        }
        console.log("VjInit");
        console.log(VjInit);

        for (i = 1; i < input_composition.length; i++) {
            for (j = (stage - 1); j >= 0; j--) {
                if (j == (stage - 1)) {
                    yInit[i][j] = (input_composition[i][1] * gas_flow) / VjInit[j];
                } else {
                    yInit[i][j] = (yInit[i][j + 1] * VjInit[j + 1]) / VjInit[j];
                }
                if (i == (input_composition.length - 1)) {
                    yInit[i][j] = 0;
                }
            }
        }
        console.log("yInit");
        console.log(yInit);

        /* cálculo de Reglas de Mezcla */

        var matrizMultiplicacionAy = createMatrix();
        var matrizMultiplicacionBy = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                matrizMultiplicacionAy[i][j] = Aij[i][j] * yInit[i][j];
                matrizMultiplicacionBy[i][j] = Bij[i][j] * yInit[i][j];
            }
        }
        var Aj = [];
        var Bj = [];
        for (j = 0; j < stage; j++) {
            var auxAy = 0;
            var auxBy = 0;
            for (i = 0; i < input_composition.length; i++) {
                auxAy += matrizMultiplicacionAy[i][j];
                auxBy += matrizMultiplicacionBy[i][j];
            }
            Aj[j] = auxAy;
            Bj[j] = auxBy;
        }
        console.log("Aj");
        console.log(Aj);
        console.log("Bj");
        console.log(Bj);

        /* START Entapía Alimentación  */

        /* START Ecuación de tercer grado */
        var a = [];
        var b = [];
        var c = [];
        var mp = [];
        var mq = [];
        var Delta = [];
        var z1 = [];
        var z2 = [];
        var z3 = [];
        for (j = 0; j < stage; j++) {
            a[j] = -1;
            b[j] = Aj[j] - Bj[j] - Math.pow(Bj[j], 2);
            c[j] = -(Aj[j] * Bj[j]);
        }
        for (j = 0; j < stage; j++) {

            mp[j] = (3 * b[j] - Math.pow(a[j], 2)) / 3;


            mq[j] = (2 * Math.pow(a[j], 3) - 9 * a[j] * b[j] + 27 * c[j]) / 27;

            Delta[j] = Math.pow(mq[j] / 2, 2) + Math.pow(mp[j] / 3, 3);
        }
        for (j = 0; j < stage; j++) {
            z1[j] = Math.pow(((-mq[j] / 2) + Math.pow(Delta[j], 1 / 2)), 1 / 3) + (-1 * Math.pow(((mq[j] / 2) + Math.pow(Delta[j], 1 / 2)), 1 / 3)) - (a[j] / 3);
        }
        for (j = 0; j < stage; j++) {
            z2[j] = - (z1[j] / 2) + Math.sqrt(Math.pow(z1[j] / 2, 2) + mq[j] / z1[j]) - a[j] / 3;
            z3[j] = - (z1[j] / 2) - Math.sqrt(Math.pow(z1[j] / 2, 2) + mq[j] / z1[j]) - a[j] / 3;
        }
        console.log("z1");
        console.log(z1);
        console.log("z2");
        console.log(z2);
        console.log("z3");
        console.log(z3);

        var phiLj = createMatrix();
        var phiVj = createMatrix();
        var kij = createMatrix();

        /* START cálculo de phi */
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                phiVj[i][j] = Math.exp((z2[j] - 1) * (Bij[i][j] / Bj[j]) - Math.log(z2[j] - Bj[j]) - (Aj[j] / Bj[j]) * ((2 * Math.pow(Aij[i][j], 0.5) / Math.pow(Aj[j], 0.5)) - (Bij[i][j]) / Bj[j])) * (Math.log((z2[j] + Bj[j]) / z2[j]));
                phiLj[i][j] = Math.exp((z1[j] - 1) * (Bij[i][j] / Bj[j]) - Math.log(z1[j] - Bj[j]) - (Aj[j] / Bj[j]) * ((2 * Math.pow(Aij[i][j], 0.5) / Math.pow(Aj[j], 0.5)) - (Bij[i][j]) / Bj[j])) * (Math.log((z1[j] + Bj[j]) / z1[j]));
            }
        }

        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                if (phiVj[i][j] == 0)
                    phiVj[i][j] = 1;
            }
        }

        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                if (phiLj[i][j] == 0)
                    phiLj[i][j] = 1;
            }
        }

        /* START cálculo Constante de equilibrio */
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                kij[i][j] = phiLj[i][j] / phiVj[i][j];
            }
        }
        console.log("phiLj");
        console.log(phiLj);
        console.log("phiVj");
        console.log(phiVj);
        console.log("kij");
        console.log(kij);

        /* END Constante de equilibrio */
        /* Inicio Algoritmo de Thomas para composiciones liquidas */
        var A = createMatrix();
        var B = createMatrix();
        var C = createMatrix();
        var D = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                if (j == (stage - 1)) { //Ultima etapa

                    A[i][j] = VjInit[j] + gas_flow - VjInit[0];
                    B[i][j] = gas_flow - VjInit[0] + (VjInit[j] * kij[i][j]);
                    C[i][j] = 0;
                    D[i][j] = -(gas_flow * input_composition[i][1]);
                }
                else { // Primera etapa e intermedias
                    A[i][j] = VjInit[j] + liquid_flow - VjInit[0];
                    D[i][j] = -(liquid_flow * input_composition[i][1]);
                    C[i][j] = VjInit[j + 1] * kij[i][j + 1];
                    B[i][j] = -(VjInit[j + 1] + liquid_flow - VjInit[0] + (VjInit[j] * kij[i][j]));
                }
            }
        }
        console.log("A");
        console.log(A);
        console.log("B");
        console.log(B);
        console.log("C");
        console.log(C);
        console.log("D");
        console.log(D);

        var qji = createMatrix();
        var pji = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                if (j == 0) { // Primera etapa
                    pji[i][j] = C[i][j] / B[i][j];
                    qji[i][j] = D[i][j] / B[i][j];
                }
                else if (j == (stage - 1)) { //Ultima Etapa
                    pji[i][j] = 0;
                    qji[i][j] = (D[i][j] - A[i][j] * pji[i][j - 1]) / (B[i][j] - A[i][j] * pji[i][j - 1]);
                }
                else { //Intermedio
                    pji[i][j] = C[i][j] / (B[i][j] - A[i][j] * pji[i][j - 1]);
                    qji[i][j] = (D[i][j] - A[i][j] * pji[i][j - 1]) / (B[i][j] - A[i][j] * pji[i][j - 1]);
                }
            }
        }
        console.log("qji");
        console.log(qji);
        console.log("pji");
        console.log(pji);

        var xji = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = (stage - 1); j > -1; j--) {
                if (j == (stage - 1)) { //Ultima Etapa
                    xji[i][j] = qji[i][j];
                }
                else { //Intermedias y primera
                    xji[i][j] = qji[i][j] - pji[i][j] * xji[i][j + 1];
                }
            }
        }
        console.log("xji");
        console.log(xji);
        /* END Algoritmo de Thomas para composiciones liquidas */

        /* START Calcula Lj0 */

        var Lj0 = [];

        for (j = 0; j < stage; j++) {
            if (j == (stage - 1)) {
                Lj0[j] = (liquid_flow + gas_flow) - VjInit[0];
            } else {
                Lj0[j] = VjInit[j + 1] + liquid_flow - VjInit[0];
            }
        }
        console.log("Lj0");
        console.log(Lj0);
        /* END Calcula Lj0 */

        /* START calculo suma x */
        var sumaX = [];
        for (j = 0; j < stage; j++) {
            sumaXinit = 0;
            for (i = 0; i < input_composition.length; i++) {
                sumaXinit += xji[i][j];
            }
            sumaX.push(sumaXinit);
        }

        var Lj = [];
        for (j = 0; j < stage; j++) {
            Lj[j] = Lj0[j] * Math.abs(sumaX[j]);
        }
        console.log("Lj");
        console.log(Lj);
        /* END calculo Lj */

        /* START Calcula Vj */
        var Vj = [];
        for (j = 0; j < stage; j++) {
            if (j == 0) {
                Vj[j] = -Lj[stage - 1] + liquid_flow + gas_flow;
            }
            else {
                Vj[j] = Lj[j - 1] - Lj[stage - 1] + gas_flow;
            }
        }
        console.log("Vj");
        console.log(Vj);
        /* END Calcula Vj */

        /*START Normalización de x & y*/
        var xNormalizado = createMatrix();
        var yNormalizado = createMatrix();
        var sumaYnorm = [];
        var sumaXnorm = [];
        var yNorm = createMatrix();
        for (j = 0; j < stage; j++) {
            sumaXAux = 0;
            for (i = 0; i < input_composition.length; i++) {
                sumaXAux += xji[i][j];
            }
            sumaXnorm.push(sumaXAux);
        }

        for (j = 0; j < stage; j++) {
            for (i = 0; i < input_composition.length; i++) {
                xNormalizado[i][j] = (xji[i][j] / sumaXnorm[j]);
            }
        }
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                yNorm[i][j] = kij[i][j] * xNormalizado[i][j];
            }
        }

        console.log("yNorm");
        console.log(yNorm);

        //Normalización de Y
        for (j = 0; j < stage; j++) {
            sumaYAux = 0;
            for (i = 0; i < input_composition.length; i++) {
                sumaYAux += yNorm[i][j];
            }
            sumaYnorm.push(sumaYAux);
        }
        for (j = 0; j < stage; j++) {
            for (i = 0; i < input_composition.length; i++) {

                yNormalizado[i][j] = (yNorm[i][j] / sumaYnorm[j]);
            }
        }
        console.log("xNormalizado");
        console.log(xNormalizado);
        console.log("yNormalizado");
        console.log(yNormalizado);

        /* END Normalización de x & y */
        tmash = [];
        tmenosh = [];
        h = [];
        for (j = 0; j < stage; j++) {
            tmash[j] = TjInit[j] + (TjInit[j] * 1e-6);
            tmenosh[j] = TjInit[j] - (TjInit[j] * 1e-6);
            h[j] = 2 * TjInit[j] * 1e-6;
        }
        console.log("tmash");
        console.log(tmash);
        console.log("tmenosh");
        console.log(tmenosh);
        console.log("h");
        console.log(h);

        /*ENTALPIA FUNCION tmenosh[j]*/
        const T0 = 0;
        const Rconst = 1.987;
        aj1ytmenosh = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                aj1ytmenosh[i][j] = ((aj0[i] * (tmenosh[j] - T0)) + (aj1[i] * ((Math.pow(tmenosh[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(tmenosh[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(tmenosh[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(tmenosh[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizado[i][j];
            }
        }
        console.log("aj1ytmenosh");
        console.log(aj1ytmenosh);

        sumaj1ktmenosh = [];
        for (j = 0; j < stage; j++) {
            iniciosumaj1ytmenosh = 0;
            for (i = 0; i < input_composition.length; i++) {
                iniciosumaj1ytmenosh += aj1ytmenosh[i][j];
            }
            sumaj1ktmenosh[j] = iniciosumaj1ytmenosh;
        }
        console.log("sumaj1ktmenosh");
        console.log(sumaj1ktmenosh);

        var aijtmenosh = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                aijtmenosh[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((tmenosh[j] / Tci[i]), 0.5)), 2);
            }
        }
        console.log("aijtmenosh");
        console.log(aijtmenosh);

        /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aijtmenosh y Bijtmenosh  en función de tmenosH*/
        var Aijtmenosh = createMatrix();
        var Bijtmenosh = createMatrix();

        /* START Realiza ecuación Aijtmenosh y Bijtmenosh*/

        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                Aijtmenosh[i][j] = (0.42747 * aijtmenosh[i][j]) * ((PjInit[j] / Pci[i]) / (Math.pow(tmenosh[j] / Tci[i], 2)));
                Bijtmenosh[i][j] = 0.08664 * ((PjInit[j] / Pci[i]) / (tmenosh[j] / Tci[i]));
            }
        }
        console.log("Aijtmenosh");
        console.log(Aijtmenosh);
        console.log("Bijtmenosh");
        console.log(Bijtmenosh);
        /* END Realiza ecuación Ajtmenosh y Bjtmenosh */

        /* cálculo de Reglas de Mezcla */

        matrizMultiplicacionAjtmenosh = createMatrix();
        matrizMultiplicacionBjtmenosh = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                matrizMultiplicacionAjtmenosh[i][j] = Aijtmenosh[i][j] * yNormalizado[i][j];
                matrizMultiplicacionBjtmenosh[i][j] = Bijtmenosh[i][j] * yNormalizado[i][j];
            }
        }

        Ajtmenosh = [];
        Bjtmenosh = [];
        for (j = 0; j < stage; j++) {
            auxAjtmenosh = 0;
            auxBjtmenosh = 0;
            for (i = 0; i < input_composition.length; i++) {
                auxAjtmenosh += matrizMultiplicacionAjtmenosh[i][j];
                auxBjtmenosh += matrizMultiplicacionBjtmenosh[i][j];
            }
            Ajtmenosh.push(auxAjtmenosh);
            Bjtmenosh.push(auxBjtmenosh);
        }

        doblesumAiktmenosh = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (k = 0; k < input_composition.length; k++) {
                for (j = 0; j < stage; j++) {
                    doblesumAiktmenosh[i][j] = yNormalizado[i][j] * yNormalizado[k][j] * Math.sqrt(Aijtmenosh[i][j] * Aijtmenosh[k][j]) * (1 - mi[i] * Math.pow(tmenosh[j] / Tci[i], 0.5) / (2 * aijtmenosh[i][j]) - mi[k] * Math.pow(tmenosh[j] / Tci[k], 0.5) / (2 * aijtmenosh[k][j]));
                }
            }
        }
        console.log("doblesumAiktmenosh");
        console.log(doblesumAiktmenosh);

        doblesumTotaltmenosh = [];
        for (j = 0; j < stage; j++) {
            iniciosumAiktmenosh = 0;
            for (i = 0; i < input_composition.length; i++) {
                iniciosumAiktmenosh += doblesumAiktmenosh[i][j];
            }
            doblesumTotaltmenosh[j] = iniciosumAiktmenosh;
        }

        HvjTmenosh = [];
        HljTmenosh = [];
        for (j = 0; j < stage; j++) {
            HvjTmenosh[j] = Rconst * sumaj1ktmenosh[j] + Rconst * tmenosh[j] * (z2[j] - 1 - (1 / Bjtmenosh[j]) * Math.log((z2[j] + Bjtmenosh[j]) / z2[j]) * (doblesumTotaltmenosh[j]));
            HljTmenosh[j] = Rconst * sumaj1ktmenosh[j] + Rconst * tmenosh[j] * (z1[j] - 1 - (1 / Bjtmenosh[j]) * Math.log((z1[j] + Bjtmenosh[j]) / z1[j]) * (doblesumTotaltmenosh[j]));
        }
        console.log("HvjTmenosh");
        console.log(HvjTmenosh);
        console.log("HljTmenosh");
        console.log(HljTmenosh);

        /*ENTALPIA FUNCION tmash[j]*/
        aj1ytmash = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                aj1ytmash[i][j] = ((aj0[i] * (tmash[j] - T0)) + (aj1[i] * ((Math.pow(tmash[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(tmash[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(tmash[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(tmash[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizado[i][j];
            }
        }
        console.log("aj1ytmash");
        console.log(aj1ytmash);

        sumaj1ktmash = [];
        for (j = 0; j < stage; j++) {
            iniciosumaj1ytmash = 0;
            for (i = 0; i < input_composition.length; i++) {
                iniciosumaj1ytmash += aj1ytmash[i][j];
            }
            sumaj1ktmash[j] = iniciosumaj1ytmash;
        }
        console.log("sumaj1ktmash");
        console.log(sumaj1ktmash);

        var aijtmash = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                aijtmash[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((tmash[j] / Tci[i]), 0.5)), 2);
            }
        }
        console.log("aijtmash");
        console.log(aijtmash);

        /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aijtmash y Bijtmash  en función de tmash*/
        var Aijtmash = createMatrix();
        var Bijtmash = createMatrix();

        /* START Realiza ecuación Aijtmash y Bijtmash*/

        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                Aijtmash[i][j] = (0.42747 * aijtmash[i][j]) * ((PjInit[j] / Pci[i]) / (Math.pow(tmash[j] / Tci[i], 2)));
                Bijtmash[i][j] = 0.08664 * ((PjInit[j] / Pci[i]) / (tmash[j] / Tci[i]));
            }
        }
        console.log("Aijtmash");
        console.log(Aijtmash);
        console.log("Bijtmash");
        console.log(Bijtmash);
        /* END Realiza ecuación Ajtmash y Bjtmash */

        /* cálculo de Reglas de Mezcla */

        matrizMultiplicacionAjtmash = createMatrix();
        matrizMultiplicacionBjtmash = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                matrizMultiplicacionAjtmash[i][j] = Aijtmash[i][j] * yNormalizado[i][j];
                matrizMultiplicacionBjtmash[i][j] = Bijtmash[i][j] * yNormalizado[i][j];
            }
        }

        Ajtmash = [];
        Bjtmash = [];
        for (j = 0; j < stage; j++) {
            auxAjtmash = 0;
            auxBjtmash = 0;
            for (i = 0; i < input_composition.length; i++) {
                auxAjtmash += matrizMultiplicacionAjtmash[i][j];
                auxBjtmash += matrizMultiplicacionBjtmash[i][j];
            }
            Ajtmash.push(auxAjtmash);
            Bjtmash.push(auxBjtmash);
        }

        doblesumAiktmash = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (k = 0; k < input_composition.length; k++) {
                for (j = 0; j < stage; j++) {
                    doblesumAiktmash[i][j] = yNormalizado[i][j] * yNormalizado[k][j] * Math.sqrt(Aijtmash[i][j] * Aijtmash[k][j]) * (1 - mi[i] * Math.pow(tmash[j] / Tci[i], 0.5) / (2 * aijtmash[i][j]) - mi[k] * Math.pow(tmash[j] / Tci[k], 0.5) / (2 * aijtmash[k][j]));
                }
            }
        }
        console.log("doblesumAiktmash");
        console.log(doblesumAiktmash);

        doblesumTotaltmash = [];
        for (j = 0; j < stage; j++) {
            iniciosumAiktmash = 0;
            for (i = 0; i < input_composition.length; i++) {
                iniciosumAiktmash += doblesumAiktmash[i][j];
            }
            doblesumTotaltmash[j] = iniciosumAiktmash;
        }

        HvjTmash = [];
        HljTmash = [];
        for (j = 0; j < stage; j++) {
            HvjTmash[j] = Rconst * sumaj1ktmash[j] + Rconst * tmash[j] * (z2[j] - 1 - (1 / Bjtmash[j]) * Math.log((z2[j] + Bjtmash[j]) / z2[j]) * (doblesumTotaltmash[j]));
            HljTmash[j] = Rconst * sumaj1ktmash[j] + Rconst * tmash[j] * (z1[j] - 1 - (1 / Bjtmash[j]) * Math.log((z1[j] + Bjtmash[j]) / z1[j]) * (doblesumTotaltmash[j]));
        }
        console.log("HvjTmash");
        console.log(HvjTmash);
        console.log("HljTmash");
        console.log(HljTmash);

        /*ENTALPIA GENERAL*/
        aj1y = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                aj1y[i][j] = ((aj0[i] * (TjInit[j] - T0)) + (aj1[i] * ((Math.pow(TjInit[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(TjInit[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(TjInit[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(TjInit[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizado[i][j];
            }
        }
        console.log("aj1y");
        console.log(aj1y);

        sumaj1k = [];
        for (j = 0; j < stage; j++) {
            iniciosumaj1y = 0;
            for (i = 0; i < input_composition.length; i++) {
                iniciosumaj1y += aj1y[i][j];
            }
            sumaj1k[j] = iniciosumaj1y;
        }
        console.log("sumaj1k");
        console.log(sumaj1k);

        sumaj1kvap = [];
        for (j = 0; j < stages; j++) {
            iniciosumaj1yvap = 0;
            for (i = 0; i < (input_composition.length - 1); i++) {
                iniciosumaj1yvap += aj1y[i][j];
            }
            sumaj1kvap[j] = iniciosumaj1yvap;
        }

        sumaj1kliq = [];
        for (j = 0; j < stages; j++) {
            sumaj1kliq[j] = aj1y[input_composition.length - 1][j];
        }

        var aij = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                aij[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((TjInit[j] / Tci[i]), 0.5)), 2);
            }
        }
        console.log("aij");
        console.log(aij);

        /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aij y Bij  en función de */
        var Aij = createMatrix();
        var Bij = createMatrix();

        /* START Realiza ecuación Aij y Bij*/

        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                Aij[i][j] = (0.42747 * aij[i][j]) * ((PjInit[j] / Pci[i]) / (Math.pow(TjInit[j] / Tci[i], 2)));
                Bij[i][j] = 0.08664 * ((PjInit[j] / Pci[i]) / (TjInit[j] / Tci[i]));
            }
        }
        console.log("Aij");
        console.log(Aij);
        console.log("Bij");
        console.log(Bij);
        /* END Realiza ecuación Aj y Bj */

        /* cálculo de Reglas de Mezcla */

        matrizMultiplicacionAj = createMatrix();
        matrizMultiplicacionBj = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (j = 0; j < stage; j++) {
                matrizMultiplicacionAj[i][j] = Aij[i][j] * yNormalizado[i][j];
                matrizMultiplicacionBj[i][j] = Bij[i][j] * yNormalizado[i][j];
            }
        }

        Aj = [];
        Bj = [];
        for (j = 0; j < stage; j++) {
            auxAj = 0;
            auxBj = 0;
            for (i = 0; i < input_composition.length; i++) {
                auxAj += matrizMultiplicacionAj[i][j];
                auxBj += matrizMultiplicacionBj[i][j];
            }
            Aj.push(auxAj);
            Bj.push(auxBj);
        }

        doblesumAik = createMatrix();
        for (i = 0; i < input_composition.length; i++) {
            for (k = 0; k < input_composition.length; k++) {
                for (j = 0; j < stage; j++) {
                    doblesumAik[i][j] = yNormalizado[i][j] * yNormalizado[k][j] * Math.sqrt(Aij[i][j] * Aij[k][j]) * (1 - mi[i] * Math.pow(TjInit[j] / Tci[i], 0.5) / (2 * aij[i][j]) - mi[k] * Math.pow(TjInit[j] / Tci[k], 0.5) / (2 * aij[k][j]));
                }
            }
        }
        console.log("doblesumAik");
        console.log(doblesumAik);

        doblesumTotal = [];
        for (j = 0; j < stage; j++) {
            iniciosumAik = 0;
            for (i = 0; i < input_composition.length; i++) {
                iniciosumAik += doblesumAik[i][j];
            }
            doblesumTotal[j] = iniciosumAik;
        }

        Hvj = [];
        Hlj = [];
        for (j = 0; j < stage; j++) {
            Hvj[j] = Rconst * sumaj1k[j] + Rconst * TjInit[j] * (z2[j] - 1 - (1 / Bj[j]) * Math.log((z2[j] + Bj[j]) / z2[j]) * (doblesumTotal[j]));
            Hlj[j] = Rconst * sumaj1k[j] + Rconst * TjInit[j] * (z1[j] - 1 - (1 / Bj[j]) * Math.log((z1[j] + Bj[j]) / z1[j]) * (doblesumTotal[j]));
        }
        console.log("Hvj");
        console.log(Hvj);
        console.log("Hlj");
        console.log(Hlj);

        /*Derivadas*/
        const F = 1;

        ecuHj = [];
        derivtj0 = [];
        derivtj1L = [];
        derivtj1V = [];
        derivtj2 = [];
        for (j = 0; j < stage; j++) {
            if (j == (stage - 1)) {
                ecuHj[j] = Lj[j - 1] * Hlj[j - 1] + gas_flow * sumaj1kvap[j] * Rconst - Lj[j] * Hlj[j] - Vj[j] * Hvj[j] - Qj[j];
                derivtj0[j] = (HljTmash[j - 1] - HljTmenosh[j - 1]) / h[j - 1];
                derivtj1L[j] = (HljTmash[j] - HljTmenosh[j]) / h[j];
                derivtj1V[j] = (HvjTmash[j] - HvjTmenosh[j]) / h[j];
            }
            else if (j == 0) {
                ecuHj[j] = Vj[j + 1] * Hvj[j + 1] + liquid_flow * sumaj1kliq[j] * Rconst - Lj[j] * Hlj[j] - Vj[j] * Hvj[j] - Qj[j];
                derivtj1L[j] = (HljTmash[j] - HljTmenosh[j]) / h[j];
                derivtj1V[j] = (HvjTmash[j] - HvjTmenosh[j]) / h[j];
                derivtj2[j] = (HvjTmash[j + 1] - HvjTmenosh[j + 1]) / h[j + 1];
            }
            else {
                ecuHj[j] = Lj[j - 1] * Hlj[j - 1] + Vj[j + 1] * Hvj[j + 1] - Lj[j] * Hlj[j] - Vj[j] * Hvj[j] - Qj[j];
                derivtj0[j] = (HljTmash[j - 1] - HljTmenosh[j - 1]) / h[j - 1];
                derivtj1L[j] = (HljTmash[j] - HljTmenosh[j]) / h[j];
                derivtj1V[j] = (HvjTmash[j] - HvjTmenosh[j]) / h[j];
                derivtj2[j] = (HvjTmash[j + 1] - HvjTmenosh[j + 1]) / h[j + 1];
            }
        }
        console.log("ecuHj");
        console.log(ecuHj);
        console.log("derivtj0");
        console.log(derivtj0);
        console.log("derivtj1L");
        console.log(derivtj1L);
        console.log("derivtj1V");
        console.log(derivtj1V);
        console.log("derivtj2");
        console.log(derivtj2);

        /* Inicio Algoritmo de Thomas para DeltaT */
        Ajener = [];
        Bjener = [];
        Cjener = [];
        Djener = [];
        for (j = 0; j < stage; j++) {
            if (j == (stage - 1)) { //Ultima etapa
                Ajener[j] = Lj[j - 1] * derivtj0[j];
                Bjener[j] = -Lj[j] * derivtj1L[j] - Vj[j] * derivtj1V[j];
                Cjener[j] = 0;
                Djener[j] = -ecuHj[j];
            }
            else if (j == 0) { //Primera etapa
                Cjener[j] = Vj[j + 1] * derivtj2[j];
                Bjener[j] = -Lj[j] * derivtj1L[j] - Vj[j] * derivtj1V[j];
                Djener[j] = -ecuHj[j];
            }
            else { //Intermedias
                Ajener[j] = Lj[j - 1] * derivtj0[j];
                Cjener[j] = Vj[j + 1] * derivtj2[j];
                Bjener[j] = -Lj[j] * derivtj1L[j] - Vj[j] * derivtj1V[j];
                Djener[j] = -ecuHj[j];
            }
        }
        console.log("Ajener");
        console.log(Ajener);
        console.log("Bjener");
        console.log(Bjener);
        console.log("Cjener");
        console.log(Cjener);
        console.log("Djener");
        console.log(Djener);

        qjiener = [];
        pjiener = [];
        for (j = 0; j < stage; j++) {

            if (j == 0) { // Primera etapa
                pjiener[j] = Cjener[j] / Bjener[j];
                qjiener[j] = Djener[j] / Bjener[j];
            }
            else if (j == (stage - 1)) { //Ultima Etapa
                pjiener[j] = 0;
                qjiener[j] = (Djener[j] - Ajener[j] * pjiener[j - 1]) / (Bjener[j] - Ajener[j] * pjiener[j - 1]);
            }
            else { //Intermedio
                pjiener[j] = Cjener[j] / (Bjener[j] - Ajener[j] * pjiener[j - 1]);
                qjiener[j] = (Djener[j] - Ajener[j] * pjiener[j - 1]) / (Bjener[j] - Ajener[j] * pjiener[j - 1]);
            }
        }
        console.log("qjiener");
        console.log(qjiener);
        console.log("pjiener");
        console.log(pjiener);

        deltaTj = [];
        for (j = (stage - 1); j > -1; j--) {
            if (j == (stage - 1)) { //Ultima Etapa
                deltaTj[j] = qjiener[j];
            }
            else { //Resto de etapas
                deltaTj[j] = qjiener[j] - pjiener[j] * deltaTj[j + 1];
            }
        }
        console.log("deltaTj");
        console.log(deltaTj);

        /* END Algoritmo de Thomas para DeltaTj */

        /* Inicia calculo T(k+1) */
        Tk = [];
        for (j = 0; j < stage; j++) {
            Tk[j] = TjInit[j] + deltaTj[j];
        }
        console.log("Tk");
        console.log(Tk);
        /* Termina calculo T(k+1) */

        /* ******************************************************************************************************************** */
        /* ******************************************************************************************************************** */
        /* ******************************************************************************************************************** */

        var Tkit = [];
        for (j = 0; j < stage; j++) {
            Tkit[j] = Tk[j];
        }
        console.log("Tkit");
        console.log(Tkit);

        var ykit = createMatrix();
        for (j = 0; j < stage; j++) {
            for (i = 0; i < input_composition.length; i++) {
                ykit[i][j] = yNormalizado[i][j];
            }
        }
        console.log("ykit");
        console.log(ykit);

        var Vjkit = [];
        for (j = 0; j < stage; j++) {
            Vjkit[j] = Vj[j];
        }
        console.log("Vjkit");
        console.log(Vjkit);

        /*Start iteration*/
        do {
            var aijit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    aijit[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((Tkit[j] / Tci[i]), 0.5)), 2);
                }
            }

            var Aijit = createMatrix();
            var Bijit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    Aijit[i][j] = (0.42747 * aijit[i][j]) * ((PjInit[j] / Pci[i]) / (Math.pow(Tkit[j] / Tci[i], 2)));
                    Bijit[i][j] = 0.08664 * ((PjInit[j] / Pci[i]) / (Tkit[j] / Tci[i]));
                }
            }
            console.log("Aijit");
            console.log(Aijit);
            console.log("Bijit");
            console.log(Bijit);

            /* cálculo de Reglas de Mezcla */
            var matrizMultiplicacionAyit = createMatrix();
            var matrizMultiplicacionByit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    matrizMultiplicacionAyit[i][j] = Aijit[i][j] * ykit[i][j];
                    matrizMultiplicacionByit[i][j] = Bijit[i][j] * ykit[i][j];
                }
            }
            var Ajit = [];
            var Bjit = [];
            for (j = 0; j < stage; j++) {
                var auxAyit = 0;
                var auxByit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    auxAyit += matrizMultiplicacionAyit[i][j];
                    auxByit += matrizMultiplicacionByit[i][j];
                }
                Ajit[j] = auxAyit;
                Bjit[j] = auxByit;
            }
            console.log("Ajit");
            console.log(Ajit);
            console.log("Bjit");
            console.log(Bjit);

            /* START Ecuación de tercer grado */
            var ait = [];
            var bit = [];
            var cit = [];
            var mpit = [];
            var mqit = [];
            var Deltait = [];
            var z1it = [];
            var z2it = [];
            var z3it = [];
            var mqitpositive = [];
            for (j = 0; j < stage; j++) {
                ait[j] = -1;
                bit[j] = Ajit[j] - Bjit[j] - Math.pow(Bjit[j], 2);
                cit[j] = -(Ajit[j] * Bjit[j]);
            }
            for (j = 0; j < stage; j++) {
                mpit[j] = (3 * bit[j] - Math.pow(ait[j], 2)) / 3;
                mqit[j] = (2 * Math.pow(ait[j], 3) - 9 * ait[j] * bit[j] + 27 * cit[j]) / 27;
                Deltait[j] = Math.pow(mqit[j] / 2, 2) + Math.pow(mpit[j] / 3, 3);
            }

            for (j = 0; j < stage; j++) {
                if (mqit[j] < 0)
                    mqit[j] = mqit[1];
            }

            for (j = 0; j < stage; j++) {
                z1it[j] = Math.pow(((-mqit[j] / 2) + Math.pow(Deltait[j], 1 / 2)), 1 / 3) + (-1 * Math.pow(((mqit[j] / 2) + Math.pow(Deltait[j], 1 / 2)), 1 / 3)) - (ait[j] / 3);
            }
            for (j = 0; j < stage; j++) {
                z2it[j] = - (z1it[j] / 2) + Math.sqrt(Math.pow(z1it[j] / 2, 2) + mqit[j] / z1it[j]) - ait[j] / 3;
                z3it[j] = - (z1it[j] / 2) - Math.sqrt(Math.pow(z1it[j] / 2, 2) + mqit[j] / z1it[j]) - ait[j] / 3;
            }
            console.log("bit");
            console.log(bit);
            console.log("mqit");
            console.log(mqit);
            console.log("z1it");
            console.log(z1it);
            console.log("z2it");
            console.log(z2it);
            console.log("z3it");
            console.log(z3it);

            var phiLjit = createMatrix();
            var phiVjit = createMatrix();
            var kijit = createMatrix();
            /* START cálculo de phi */
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    phiVjit[i][j] = Math.exp((z2it[j] - 1) * (Bijit[i][j] / Bjit[j]) - Math.log(Math.abs(z2it[j] - Bjit[j])) - (Ajit[j] / Bjit[j]) * ((2 * Math.pow(Aijit[i][j], 0.5) / Math.pow(Ajit[j], 0.5)) - (Bijit[i][j]) / Bjit[j])) * (Math.log((z2it[j] + Bjit[j]) / z2it[j]));
                    phiLjit[i][j] = Math.exp((z1it[j] - 1) * (Bijit[i][j] / Bjit[j]) - Math.log(Math.abs(z1it[j] - Bjit[j])) - (Ajit[j] / Bjit[j]) * ((2 * Math.pow(Aijit[i][j], 0.5) / Math.pow(Ajit[j], 0.5)) - (Bijit[i][j]) / Bjit[j])) * (Math.log((z1it[j] + Bjit[j]) / z1it[j]));
                }
            }
            console.log("phiVjit");
            console.log(phiVjit);
            console.log("phiLjit");
            console.log(phiLjit);

            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    if (phiVjit[i][j] == 0)
                        phiVjit[i][j] = 1;
                }
            }

            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    if (phiLjit[i][j] == 0)
                        phiLjit[i][j] = 1;
                }
            }

            /* START cálculo Constante de equilibrio */
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    kijit[i][j] = phiLjit[i][j] / phiVjit[i][j];
                }
            }
            console.log("kijit");
            console.log(kijit);
            /* END Constante de equilibrio */

            /* Inicio Algoritmo de Thomas para composiciones liquidas */
            var Ait = createMatrix();
            var Bit = createMatrix();
            var Cit = createMatrix();
            var Dit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    if (j == (stage - 1)) { //Ultima etapa

                        Ait[i][j] = Vjkit[j] + gas_flow - Vjkit[0];
                        Bit[i][j] = gas_flow - Vjkit[0] + (Vjkit[j] * kijit[i][j]);
                        Cit[i][j] = 0;
                        Dit[i][j] = -(gas_flow * input_composition[i][1]);
                    }
                    else { // Primera etapa e intermedias
                        Ait[i][j] = Vjkit[j] + liquid_flow - Vjkit[0];
                        Dit[i][j] = -(liquid_flow * input_composition[i][1]);
                        Cit[i][j] = Vjkit[j + 1] * kijit[i][j + 1];
                        Bit[i][j] = -(Vjkit[j + 1] + liquid_flow - Vjkit[0] + (Vjkit[j] * kijit[i][j]));
                    }
                }
            }

            var qjiit = createMatrix();
            var pjiit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    if (j == 0) { // Primera etapa
                        pjiit[i][j] = Cit[i][j] / Bit[i][j];
                        qjiit[i][j] = Dit[i][j] / Bit[i][j];
                    }
                    else if (j == (stage - 1)) { //Ultima Etapa
                        pjiit[i][j] = 0;
                        qjiit[i][j] = (Dit[i][j] - Ait[i][j] * pjiit[i][j - 1]) / (Bit[i][j] - Ait[i][j] * pjiit[i][j - 1]);
                    }
                    else { //Intermedio
                        pjiit[i][j] = Cit[i][j] / (Bit[i][j] - Ait[i][j] * pji[i][j - 1]);
                        qjiit[i][j] = (Dit[i][j] - Ait[i][j] * pjiit[i][j - 1]) / (Bit[i][j] - Ait[i][j] * pjiit[i][j - 1]);
                    }
                }
            }

            var xjiit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = (stage - 1); j > -1; j--) {
                    if (j == (stage - 1)) { //Ultima Etapa
                        xjiit[i][j] = qjiit[i][j];
                    }
                    else { //Intermedias y primera
                        xjiit[i][j] = qjiit[i][j] - pjiit[i][j] * xjiit[i][j + 1];
                    }
                }
            }
            console.log("xjiit");
            console.log(xjiit);
            /* END Algoritmo de Thomas para composiciones liquidas */


            /* START Calcula Lj0it */
            var Lj0it = [];
            for (j = 0; j < stage; j++) {
                if (j == (stage - 1)) {
                    Lj0it[j] = (liquid_flow + gas_flow) - Vjkit[0];
                } else {
                    Lj0it[j] = Vjkit[j + 1] + liquid_flow - Vjkit[0];
                }
            }
            /* END Calcula Lj0it */

            /* START calculo suma x */
            var sumaXit = [];
            for (j = 0; j < stage; j++) {
                sumaXinitit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    sumaXinitit += xjiit[i][j];
                }
                sumaXit.push(sumaXinitit);
            }

            var Ljit = [];
            for (j = 0; j < stage; j++) {
                Ljit[j] = Lj0it[j] * Math.abs(sumaXit[j]);
            }
            /* END calculo Ljit */

            /* START Calcula Vjit */
            var Vjit = [];
            for (j = 0; j < stage; j++) {
                if (j == 0) {
                    Vjit[j] = -Ljit[stage - 1] + liquid_flow + gas_flow;
                }
                else {
                    Vjit[j] = Ljit[j - 1] - Ljit[stage - 1] + gas_flow;
                }
            }
            /* END Calcula Vj */

            /*START Normalización de x & y*/
            var xNormalizadoit = createMatrix();
            var yNormalizadoit = createMatrix();
            var sumaYnormit = [];
            var sumaXnormit = [];
            var yNormit = createMatrix();
            for (j = 0; j < stage; j++) {
                sumaXAuxit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    sumaXAuxit += xjiit[i][j];
                }
                sumaXnormit.push(sumaXAuxit);
            }

            for (j = 0; j < stage; j++) {
                for (i = 0; i < input_composition.length; i++) {
                    xNormalizadoit[i][j] = (xjiit[i][j] / sumaXnormit[j]);
                }
            }
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    yNormit[i][j] = kijit[i][j] * xNormalizadoit[i][j];
                }
            }

            //Normalización de Y
            for (j = 0; j < stage; j++) {
                sumaYAuxit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    sumaYAuxit += yNormit[i][j];
                }
                sumaYnormit.push(sumaYAuxit);
            }
            for (j = 0; j < stage; j++) {
                for (i = 0; i < input_composition.length; i++) {

                    yNormalizadoit[i][j] = (yNormit[i][j] / sumaYnormit[j]);
                }
            }
            /* END Normalización de x & y */

            tmashit = [];
            tmenoshit = [];
            hit = [];
            for (j = 0; j < stage; j++) {
                tmashit[j] = Tkit[j] + (Tkit[j] * 1e-6);
                tmenoshit[j] = Tkit[j] - (Tkit[j] * 1e-6);
                hit[j] = 2 * Tkit[j] * 1e-6;
            }

            /*ENTALPIA FUNCION tmenosh[j]*/
            const T0 = 0;
            const Rconst = 1.987;
            aj1ytmenoshit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    aj1ytmenoshit[i][j] = ((aj0[i] * (tmenoshit[j] - T0)) + (aj1[i] * ((Math.pow(tmenoshit[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(tmenoshit[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(tmenoshit[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(tmenoshit[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizadoit[i][j];
                }
            }

            sumaj1ktmenoshit = [];
            for (j = 0; j < stage; j++) {
                iniciosumaj1ytmenoshit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    iniciosumaj1ytmenoshit += aj1ytmenoshit[i][j];
                }
                sumaj1ktmenoshit[j] = iniciosumaj1ytmenoshit;
            }

            var aijtmenoshit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    aijtmenoshit[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((tmenoshit[j] / Tci[i]), 0.5)), 2);
                }
            }

            /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aijtmenosh y Bijtmenosh  en función de tmenosH*/
            var Aijtmenoshit = createMatrix();
            var Bijtmenoshit = createMatrix();

            /* START Realiza ecuación Aijtmenosh y Bijtmenosh*/
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    Aijtmenoshit[i][j] = (0.42747 * aijtmenoshit[i][j]) * ((PjInit[j] / Pci[i]) / (Math.pow(tmenoshit[j] / Tci[i], 2)));
                    Bijtmenoshit[i][j] = 0.08664 * ((PjInit[j] / Pci[i]) / (tmenoshit[j] / Tci[i]));
                }
            }
            /* END Realiza ecuación Ajtmenosh y Bjtmenosh */

            /* cálculo de Reglas de Mezcla */

            matrizMultiplicacionAjtmenoshit = createMatrix();
            matrizMultiplicacionBjtmenoshit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    matrizMultiplicacionAjtmenoshit[i][j] = Aijtmenoshit[i][j] * yNormalizadoit[i][j];
                    matrizMultiplicacionBjtmenoshit[i][j] = Bijtmenoshit[i][j] * yNormalizadoit[i][j];
                }
            }

            Ajtmenoshit = [];
            Bjtmenoshit = [];
            for (j = 0; j < stage; j++) {
                auxAjtmenoshit = 0;
                auxBjtmenoshit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    auxAjtmenoshit += matrizMultiplicacionAjtmenoshit[i][j];
                    auxBjtmenoshit += matrizMultiplicacionBjtmenoshit[i][j];
                }
                Ajtmenoshit.push(auxAjtmenoshit);
                Bjtmenoshit.push(auxBjtmenoshit);
            }

            doblesumAiktmenoshit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (k = 0; k < input_composition.length; k++) {
                    for (j = 0; j < stage; j++) {
                        doblesumAiktmenoshit[i][j] = yNormalizadoit[i][j] * yNormalizadoit[k][j] * Math.sqrt(Aijtmenoshit[i][j] * Aijtmenoshit[k][j]) * (1 - mi[i] * Math.pow(tmenoshit[j] / Tci[i], 0.5) / (2 * aijtmenoshit[i][j]) - mi[k] * Math.pow(tmenoshit[j] / Tci[k], 0.5) / (2 * aijtmenoshit[k][j]));
                    }
                }
            }

            doblesumTotaltmenoshit = [];
            for (j = 0; j < stage; j++) {
                iniciosumAiktmenoshit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    iniciosumAiktmenoshit += doblesumAiktmenoshit[i][j];
                }
                doblesumTotaltmenoshit[j] = iniciosumAiktmenoshit;
            }

            HvjTmenoshit = [];
            HljTmenoshit = [];
            for (j = 0; j < stage; j++) {
                HvjTmenoshit[j] = Rconst * sumaj1ktmenoshit[j] + Rconst * tmenoshit[j] * (z2it[j] - 1 - (1 / Bjtmenoshit[j]) * Math.log((z2it[j] + Bjtmenoshit[j]) / z2it[j]) * (doblesumTotaltmenoshit[j]));
                HljTmenoshit[j] = Rconst * sumaj1ktmenoshit[j] + Rconst * tmenoshit[j] * (z1it[j] - 1 - (1 / Bjtmenoshit[j]) * Math.log((z1it[j] + Bjtmenoshit[j]) / z1it[j]) * (doblesumTotaltmenoshit[j]));
            }

            /*ENTALPIA FUNCION tmash[j]*/
            aj1ytmashit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    aj1ytmashit[i][j] = ((aj0[i] * (tmashit[j] - T0)) + (aj1[i] * ((Math.pow(tmashit[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(tmashit[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(tmashit[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(tmashit[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizadoit[i][j];
                }
            }

            sumaj1ktmashit = [];
            for (j = 0; j < stage; j++) {
                iniciosumaj1ytmashit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    iniciosumaj1ytmashit += aj1ytmashit[i][j];
                }
                sumaj1ktmashit[j] = iniciosumaj1ytmashit;
            }

            var aijtmashit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    aijtmashit[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((tmashit[j] / Tci[i]), 0.5)), 2);
                }
            }

            /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aijtmash y Bijtmash  en función de tmash*/
            var Aijtmashit = createMatrix();
            var Bijtmashit = createMatrix();

            /* START Realiza ecuación Aijtmash y Bijtmash*/

            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    Aijtmashit[i][j] = (0.42747 * aijtmashit[i][j]) * ((PjInit[j] / Pci[i]) / (Math.pow(tmashit[j] / Tci[i], 2)));
                    Bijtmashit[i][j] = 0.08664 * ((PjInit[j] / Pci[i]) / (tmashit[j] / Tci[i]));
                }
            }
            /* END Realiza ecuación Ajtmash y Bjtmash */

            /* Cálculo de Reglas de Mezcla */
            matrizMultiplicacionAjtmashit = createMatrix();
            matrizMultiplicacionBjtmashit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    matrizMultiplicacionAjtmashit[i][j] = Aijtmashit[i][j] * yNormalizadoit[i][j];
                    matrizMultiplicacionBjtmashit[i][j] = Bijtmashit[i][j] * yNormalizadoit[i][j];
                }
            }

            Ajtmashit = [];
            Bjtmashit = [];
            for (j = 0; j < stage; j++) {
                auxAjtmashit = 0;
                auxBjtmashit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    auxAjtmashit += matrizMultiplicacionAjtmashit[i][j];
                    auxBjtmashit += matrizMultiplicacionBjtmashit[i][j];
                }
                Ajtmashit.push(auxAjtmashit);
                Bjtmashit.push(auxBjtmashit);
            }

            doblesumAiktmashit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (k = 0; k < input_composition.length; k++) {
                    for (j = 0; j < stage; j++) {
                        doblesumAiktmashit[i][j] = yNormalizadoit[i][j] * yNormalizadoit[k][j] * Math.sqrt(Aijtmashit[i][j] * Aijtmashit[k][j]) * (1 - mi[i] * Math.pow(tmashit[j] / Tci[i], 0.5) / (2 * aijtmashit[i][j]) - mi[k] * Math.pow(tmashit[j] / Tci[k], 0.5) / (2 * aijtmashit[k][j]));
                    }
                }
            }

            doblesumTotaltmashit = [];
            for (j = 0; j < stage; j++) {
                iniciosumAiktmashit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    iniciosumAiktmashit += doblesumAiktmashit[i][j];
                }
                doblesumTotaltmashit[j] = iniciosumAiktmashit;
            }

            HvjTmashit = [];
            HljTmashit = [];
            for (j = 0; j < stage; j++) {
                HvjTmashit[j] = Rconst * sumaj1ktmashit[j] + Rconst * tmashit[j] * (z2it[j] - 1 - (1 / Bjtmashit[j]) * Math.log((z2it[j] + Bjtmashit[j]) / z2it[j]) * (doblesumTotaltmashit[j]));
                HljTmashit[j] = Rconst * sumaj1ktmashit[j] + Rconst * tmashit[j] * (z1it[j] - 1 - (1 / Bjtmashit[j]) * Math.log((z1it[j] + Bjtmashit[j]) / z1it[j]) * (doblesumTotaltmashit[j]));
            }

            /*ENTALPIA GENERAL*/
            aj1yit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    aj1yit[i][j] = ((aj0[i] * (Tkit[j] - T0)) + (aj1[i] * ((Math.pow(Tkit[j], 2) - Math.pow(T0, 2)) / 2)) + (aj2[i] * ((Math.pow(Tkit[j], 3) - Math.pow(T0, 3)) / 3)) + (aj3[i] * ((Math.pow(Tkit[j], 4) - Math.pow(T0, 4)) / 4)) + (aj4[i] * ((Math.pow(Tkit[j], 5) - Math.pow(T0, 5)) / 5))) * yNormalizadoit[i][j];
                }
            }

            sumaj1kit = [];
            for (j = 0; j < stage; j++) {
                iniciosumaj1yit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    iniciosumaj1yit += aj1yit[i][j];
                }
                sumaj1kit[j] = iniciosumaj1yit;
            }

            sumaj1kitvap = [];
            for (j = 0; j < stages; j++) {
                iniciosumaj1yitvap = 0;
                for (i = 0; i < (input_composition.length - 1); i++) {
                    iniciosumaj1yitvap += aj1yit[i][j];
                }
                sumaj1kitvap[j] = iniciosumaj1yitvap;
            }

            sumaj1kitliq = [];
            for (j = 0; j < stages; j++) {
                sumaj1kitliq[j] = aj1yit[input_composition.length - 1][j];
            }

            var aijit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    aijit[i][j] = Math.pow(1 + mi[i] * (1 - Math.pow((Tkit[j] / Tci[i]), 0.5)), 2);
                }
            }

            /* Crea el arreglo de acuerdo con las etapas y componentes seleccionados para calcular Aij y Bij  en función de */
            var Aijit = createMatrix();
            var Bijit = createMatrix();

            /* START Realiza ecuación Aij y Bij*/
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    Aijit[i][j] = (0.42747 * aijit[i][j]) * ((PjInit[j] / Pci[i]) / (Math.pow(Tkit[j] / Tci[i], 2)));
                    Bijit[i][j] = 0.08664 * ((PjInit[j] / Pci[i]) / (Tkit[j] / Tci[i]));
                }
            }
            /* END Realiza ecuación Aj y Bj */

            /* cálculo de Reglas de Mezcla */

            matrizMultiplicacionAjit = createMatrix();
            matrizMultiplicacionBjit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (j = 0; j < stage; j++) {
                    matrizMultiplicacionAjit[i][j] = Aijit[i][j] * yNormalizadoit[i][j];
                    matrizMultiplicacionBjit[i][j] = Bijit[i][j] * yNormalizadoit[i][j];
                }
            }

            Ajit = [];
            Bjit = [];
            for (j = 0; j < stage; j++) {
                auxAjit = 0;
                auxBjit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    auxAjit += matrizMultiplicacionAjit[i][j];
                    auxBjit += matrizMultiplicacionBjit[i][j];
                }
                Ajit.push(auxAjit);
                Bjit.push(auxBjit);
            }

            doblesumAikit = createMatrix();
            for (i = 0; i < input_composition.length; i++) {
                for (k = 0; k < input_composition.length; k++) {
                    for (j = 0; j < stage; j++) {
                        doblesumAikit[i][j] = yNormalizadoit[i][j] * yNormalizadoit[k][j] * Math.sqrt(Aijit[i][j] * Aijit[k][j]) * (1 - mi[i] * Math.pow(Tkit[j] / Tci[i], 0.5) / (2 * aijit[i][j]) - mi[k] * Math.pow(Tkit[j] / Tci[k], 0.5) / (2 * aijit[k][j]));
                    }
                }
            }

            doblesumTotalit = [];
            for (j = 0; j < stage; j++) {
                iniciosumAikit = 0;
                for (i = 0; i < input_composition.length; i++) {
                    iniciosumAikit += doblesumAikit[i][j];
                }
                doblesumTotalit[j] = iniciosumAikit;
            }

            Hvjit = [];
            Hljit = [];
            for (j = 0; j < stage; j++) {
                Hvjit[j] = Rconst * sumaj1kit[j] + Rconst * Tkit[j] * (z2it[j] - 1 - (1 / Bjit[j]) * Math.log((z2it[j] + Bjit[j]) / z2it[j]) * (doblesumTotalit[j]));
                Hljit[j] = Rconst * sumaj1kit[j] + Rconst * Tkit[j] * (z1it[j] - 1 - (1 / Bjit[j]) * Math.log((z1it[j] + Bjit[j]) / z1it[j]) * (doblesumTotalit[j]));
            }

            /*Derivadas*/
            ecuHjit = [];
            derivtj0it = [];
            derivtj1Lit = [];
            derivtj1Vit = [];
            derivtj2it = [];
            for (j = 0; j < stage; j++) {
                if (j == (stage - 1)) {
                    ecuHjit[j] = Ljit[j - 1] * Hljit[j - 1] + gas_flow * sumaj1kitvap[j] * Rconst - Ljit[j] * Hljit[j] - Vjit[j] * Hvjit[j] - Qj[j];
                    derivtj0it[j] = (HljTmashit[j - 1] - HljTmenoshit[j - 1]) / hit[j - 1];
                    derivtj1Lit[j] = (HljTmashit[j] - HljTmenoshit[j]) / hit[j];
                    derivtj1Vit[j] = (HvjTmashit[j] - HvjTmenoshit[j]) / hit[j];
                }
                else if (j == 0) {
                    ecuHjit[j] = Vjit[j + 1] * Hvjit[j + 1] + liquid_flow * sumaj1kitliq[j] * Rconst - Ljit[j] * Hljit[j] - Vjit[j] * Hvjit[j] - Qj[j];
                    derivtj1Lit[j] = (HljTmashit[j] - HljTmenoshit[j]) / hit[j];
                    derivtj1Vit[j] = (HvjTmashit[j] - HvjTmenoshit[j]) / hit[j];
                    derivtj2it[j] = (HvjTmashit[j + 1] - HvjTmenoshit[j + 1]) / hit[j + 1];
                }
                else {
                    ecuHjit[j] = Ljit[j - 1] * Hljit[j - 1] + Vjit[j + 1] * Hvjit[j + 1] - Ljit[j] * Hljit[j] - Vjit[j] * Hvjit[j] - Qj[j];
                    derivtj0it[j] = (HljTmashit[j - 1] - HljTmenoshit[j - 1]) / hit[j - 1];
                    derivtj1Lit[j] = (HljTmashit[j] - HljTmenoshit[j]) / hit[j];
                    derivtj1Vit[j] = (HvjTmashit[j] - HvjTmenoshit[j]) / hit[j];
                    derivtj2it[j] = (HvjTmashit[j + 1] - HvjTmenoshit[j + 1]) / hit[j + 1];
                }
            }

            /* Inicio Algoritmo de Thomas para DeltaT */
            Ajenerit = [];
            Bjenerit = [];
            Cjenerit = [];
            Djenerit = [];
            for (j = 0; j < stage; j++) {
                if (j == (stage - 1)) { //Ultima etapa
                    Ajenerit[j] = Ljit[j - 1] * derivtj0it[j];
                    Bjenerit[j] = -Ljit[j] * derivtj1Lit[j] - Vjit[j] * derivtj1Vit[j];
                    Cjenerit[j] = 0;
                    Djenerit[j] = -ecuHjit[j];
                }
                else if (j == 0) { //Primera etapa
                    Cjenerit[j] = Vjit[j + 1] * derivtj2it[j];
                    Bjenerit[j] = -Ljit[j] * derivtj1Lit[j] - Vjit[j] * derivtj1Vit[j];
                    Djenerit[j] = -ecuHjit[j];
                }
                else { //Intermedias
                    Ajenerit[j] = Ljit[j - 1] * derivtj0it[j];
                    Cjenerit[j] = Vjit[j + 1] * derivtj2it[j];
                    Bjenerit[j] = -Ljit[j] * derivtj1Lit[j] - Vjit[j] * derivtj1Vit[j];
                    Djenerit[j] = -ecuHjit[j];
                }
            }

            qjienerit = [];
            pjienerit = [];
            for (j = 0; j < stage; j++) {

                if (j == 0) { // Primera etapa
                    pjienerit[j] = Cjenerit[j] / Bjenerit[j];
                    qjienerit[j] = Djenerit[j] / Bjenerit[j];
                }
                else if (j == (stage - 1)) { //Ultima Etapa
                    pjienerit[j] = 0;
                    qjienerit[j] = (Djenerit[j] - Ajenerit[j] * pjienerit[j - 1]) / (Bjenerit[j] - Ajenerit[j] * pjienerit[j - 1]);
                }
                else { //Intermedio
                    pjienerit[j] = Cjenerit[j] / (Bjenerit[j] - Ajenerit[j] * pjienerit[j - 1]);
                    qjienerit[j] = (Djenerit[j] - Ajenerit[j] * pjienerit[j - 1]) / (Bjenerit[j] - Ajenerit[j] * pjienerit[j - 1]);
                }
            }

            deltaTjit = [];
            for (j = (stage - 1); j > -1; j--) {
                if (j == (stage - 1)) { //Ultima Etapa
                    deltaTjit[j] = qjienerit[j];
                }
                else { //Resto de etapas
                    deltaTjit[j] = qjienerit[j] - pjienerit[j] * deltaTjit[j + 1];
                }
            }
            /* END Algoritmo de Thomas para DeltaTj */

            /* Inicia calculo T(k+1) y convergencia */
            for (j = 0; j < stage; j++) {
                Tkit[j] = Tkit[j] + deltaTjit[j];
            }

            taoj = [];
            for (j = 0; j < stage; j++) {
                taoj[j] = Math.pow(deltaTjit[j], 2);
            }

            var conv_criteria;
            initsumConv = 0;
            for (j = 0; j < stage; j++) {
                initsumConv += taoj[j];
            }
            conv_criteria = initsumConv;
            /* Termina calculo T(k+1) y convergencia */

        }
        while (conv_criteria <= (0.01 * stage));
        imprimirResultados(input_composition, stages, xInit, kij, tj, yNormalizado, vj, lj, pj);
        console.log("conv_criteria");
        console.log(conv_criteria);

        /* ********************************************************************************************************************* */
        /* ********************************************************************************************************************* */
        /* ********************************************************************************************************************* */

        function createMatrix() {
            var matriz = new Array(input_composition.length);
            for (var i = 0; i < matriz.length; i++) {
                matriz[i] = new Array(parseInt(stage));
            }
            return matriz;
        }



    }else {
        toastr.error('Check all inputs', 'Error!');

    } // END Validation






        var end_time = Date.now();
        console.log("Tiempo de ejecución: " + (end_time - start_time) + " ms.");
        $("#button_simular").html('Simular');
    }, delayInMilliseconds);


}

$(document).ready(function () {
    $("#metano_checkbox").change(function () {
        if (this.checked) {
            $("#metano_input").prop('disabled', false);
        } else {
            $("#metano_input").prop('disabled', true).val("0.0");
        }
    });
    $("#etano_checkbox").change(function () {
        if (this.checked) {
            $("#etano_input").prop('disabled', false);
        } else {
            $("#etano_input").prop('disabled', true).val("0.0");
        }
    });
    $("#propano_checkbox").change(function () {
        if (this.checked) {
            $("#propano_input").prop('disabled', false);
        } else {
            $("#propano_input").prop('disabled', true).val("0.0");
        }
    });
    $("#butano_checkbox").change(function () {
        if (this.checked) {
            $("#butano_input").prop('disabled', false);
        } else {
            $("#butano_input").prop('disabled', true).val("0.0");
        }
    });
    $("#pentano_checkbox").change(function () {
        if (this.checked) {
            $("#pentano_input").prop('disabled', false);
        } else {
            $("#pentano_input").prop('disabled', true).val("0.0");
        }
    });

    $("#sulfuro_de_hidrogeno_checkbox").change(function () {
        if (this.checked) {
            $("#sulfuro_de_hidrogeno_input").prop('disabled', false);
        } else {
            $("#sulfuro_de_hidrogeno_input").prop('disabled', true).val("0.0");
        }
    });

    $("#dioxido_de_carbono_checkbox").change(function () {
        if (this.checked) {
            $("#dioxido_de_carbono_input").prop('disabled', false);
        } else {
            $("#dioxido_de_carbono_input").prop('disabled', true).val("0.0");
        }
    });
    $("#propileno_checkbox").change(function () {
        if (this.checked) {
            $("#propileno_input").prop('disabled', false);
        } else {
            $("#propileno_input").prop('disabled', true).val("0.0");
        }
    });

    $("#limpiar_campos").click(function () {
        $("#flujo_solvente").val('');
        $("#temperatura_solvente").val('');
        $("#presion_solvente").val('');
        $("#flujo_gas").val('');
        $("#temperatura_gas").val('');
        $("#presion_gas").val('');
        $("#metano_input").val('').prop('disabled', true);
        $("#etano_input").val('').prop('disabled', true);
        $("#propano_input").val('').prop('disabled', true);
        $("#butano_input").val('').prop('disabled', true);
        $("#pentano_input").val('').prop('disabled', true);
        $("#sulfuro_de_hidrogeno_input").val('').prop('disabled', true);
        $("#dioxido_de_carbono_input").val('').prop('disabled', true);
        $("#propileno_input").val('').prop('disabled', true);

        $("#presion_torre_inicial").val('');
        $("#presion_torre_final").val('');
        $("#numero_etapas").val('2');
        $('input:checkbox').removeAttr('checked');
    });

    $("#addEtapa").click(function () {
        if (parseInt($("#numero_etapas").val()) < 15) {
            $("#numero_etapas").val((parseInt($("#numero_etapas").val()) + 1));
            var etapas = $("#numero_etapas").val();
            etapas = parseInt(etapas);
            aux = ""
            for (var i = 1; i < (etapas + 1); i++) {
                aux += `                                                            <tr>
            <td>`+ i + `</td>
            <td>
                <div class="form-group" style="margin-bottom: 0px; margin-top: 1px;">
                    <input type="number" class="form-control"
                        value="0.0" id="etapa_`+ i + `">
                </div>
            </td>
        </tr>`;
            }
            $("#tablaEtapas").html(aux);
        }
    });
    $("#delEtapa").click(function () {
        if (parseInt($("#numero_etapas").val()) > 2) {
            $("#numero_etapas").val((parseInt($("#numero_etapas").val()) - 1));
            var etapas = $("#numero_etapas").val();
            etapas = parseInt(etapas);
            aux = ""
            for (var i = 1; i < (etapas + 1); i++) {
                aux += `                                                            <tr>
                <td>`+ i + `</td>
                <td>
                    <div class="form-group" style="margin-bottom: 0px; margin-top: 1px;">
                        <input type="number" class="form-control"
                            value="0.0" id="etapa_`+ i + `">
                    </div>
                </td>
            </tr>`;
            }
            $("#tablaEtapas").html(aux);
        };

    });

    $("#limpiarResultados").click(function () {
        $("#resultados").css("display", "none");
    });
    
    $("#numero_etapas").change(function updateValue() {
        var etapas = $("#numero_etapas").val();
        etapas = parseInt(etapas);
        aux = ""
        for (var i = 1; i < (etapas + 1); i++) {
            aux += `                                                            <tr>
            <td>`+ i + `</td>
            <td>
                <div class="form-group" style="margin-bottom: 0px; margin-top: 1px;">
                    <input type="number" class="form-control"
                        value="0.0" id="etapa_`+ i + `">
                </div>
            </td>
        </tr>`;
        }
        $("#tablaEtapas").html(aux);
    });




});


function imprimirResultados(componenetesEntrada, etapas, xNormalizados, kij, tj, yNormalizados, vj2, lj2, pj) {
    $("#resultados").css("display", "flex");
    var compositionTitleX = '<tr><th scope="col">Etapas</th>';
    var compositionTitleY = '<tr><th scope="col">Etapas</th>';
    var compositionTitleKij = '<tr><th scope="col">Etapas</th>';
    var compositionBodyX = '';
    var compositionBodyY = '';
    var compositionBodyKij = '';
    for (i = 0; i < componenetesEntrada.length; i++) {
        compositionTitleX += '<th scope="col">' + componenetesEntrada[i][0] + '</th>';
        compositionTitleY += '<th scope="col">' + componenetesEntrada[i][0] + '</th>';
        compositionTitleKij += '<th scope="col">' + componenetesEntrada[i][0] + '</th>';
    }
    for (j = 0; j < etapas; j++) {
        compositionBodyX += '<tr><td>' + (j + 1) + '</td>';
        compositionBodyY += '<tr><td>' + (j + 1) + '</td>';
        compositionBodyKij += '<tr><td>' + (j + 1) + '</td>';
        for (i = 0; i < componenetesEntrada.length; i++) {
            compositionBodyX += '<td>' + xNormalizados[i][j].toFixed(8) + '</td>';
            compositionBodyY += '<td>' + yNormalizados[i][j].toFixed(8) + '</td>';
            compositionBodyKij += '<td>' + kij[i][j].toFixed(8) + '</td>';
        }
        compositionBodyX += '</tr>';
        compositionBodyY += '</tr>';
        compositionBodyKij += '</tr>';
    }

    $("#resultCompositionTitleX").html(compositionTitleX + '</tr>');
    $("#resultCompositionBodyX").html(compositionBodyX);
    $("#resultCompositionTitleY").html(compositionTitleY + '</tr>');
    $("#resultCompositionBodyY").html(compositionBodyY);
    $("#resultCompositionTitleKij").html(compositionTitleKij + '</tr>');
    $("#resultCompositionBodyKij").html(compositionBodyKij);

    titulotj = '<tr><th>Etapa</th><th>Temperatura (°F)</th></tr>';
    cuerpotj = '';
    titulopj = '<tr><th>Etapa</th><th>Presión (psia)</th></tr>';
    cuerpopj = '';
    titulovj = '<tr><th>Etapa</th><th>Flujo vapor (lbmol/hr)</th></tr>';
    cuerpovj = '';
    titulolj = '<tr><th>Etapa</th><th>Flujo líquido (lbmol/hr)</th></tr>';
    cuerpolj = '';

    for (j = 0; j < etapas; j++) {
        cuerpotj += '<tr><th>' + (j + 1) + '</th><td>' + tj[j] + '</td></tr>';


        cuerpovj += '<tr><th>' + (j + 1) + '</th><td>' + vj[j] + '</td></tr>';

        cuerpolj += '<tr><th>' + (j + 1) + '</th><td>' + lj[j] + '</td></tr>';

        cuerpopj +='<tr><th>' + (j + 1) + '</th><td>' + pj[j] + '</td></tr>';
    }


    $("#resultTemperatureTitle").html(titulotj);
    $("#resultTemperatureBody").html(cuerpotj);

    $("#resultVjTitle").html(titulovj);
    $("#resultVjBody").html(cuerpovj);

    $("#resultLjTitle").html(titulolj);
    $("#resultLjBody").html(cuerpolj);

    $("#resultPjTitle").html(titulopj);
    $("#resultPjBody").html(cuerpopj);


    etapasVector = [];

    for (j = 0; j < etapas; j++) {
        etapasVector.push((j + 1));
    }

    var datosgraficaLiquido = [];
    var datosgraficaVapor = [];
    var datosgraficaKij = [];
    for (i = 0; i < componenetesEntrada.length; i++) {
        datos = [];
        datosVapor = [];
        datosKij = [];
        for (j = 0; j < etapas; j++) {
            datos.push([xNormalizados[i][j]]);
            datosVapor.push([yNormalizados[i][j]]);
            datosKij.push([kij[i][j]]);
        }
        datosgraficaLiquido.push({
            label: componenetesEntrada[i][0],
            lineTension: 0.3,
            backgroundColor: "rgba(0, 0, 0, 0.0)",
            borderColor: componenetesEntrada[i][2],
            pointRadius: 3,
            pointBackgroundColor: componenetesEntrada[i][2],
            pointBorderColor: componenetesEntrada[i][2],
            data: datos
        });

        datosgraficaVapor.push({
            label: componenetesEntrada[i][0],
            lineTension: 0.3,
            backgroundColor: "rgba(0, 0, 0, 0.0)",
            borderColor: componenetesEntrada[i][2],
            pointRadius: 3,
            pointBackgroundColor: componenetesEntrada[i][2],
            pointBorderColor: componenetesEntrada[i][2],
            data: datosVapor
        });
        datosgraficaKij.push({
            label: componenetesEntrada[i][0],
            lineTension: 0.3,
            backgroundColor: "rgba(0, 0, 0, 0.0)",
            borderColor: componenetesEntrada[i][2],
            pointRadius: 3,
            pointBackgroundColor: componenetesEntrada[i][2],
            pointBorderColor: componenetesEntrada[i][2],
            data: datosKij
        });
    }
    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';
    $("#bodyCardX").html(`<canvas id="xNormalizadoChart"></canvas>`);
    $("#bodyCardY").html(`<canvas id="yNormalizadoChart"></canvas>`);
    $("#bodyCardtj").html(`<canvas id="temperaturaChart"></canvas>`);
    $("#bodyCardlj").html(`<canvas id="ljChart"></canvas>`);
    $("#bodyCardvj").html(`<canvas id="vjChart"></canvas>`);
    $("#bodyCardpj").html(`<canvas id="pjChart"></canvas>`);
    $("#bodyCardk").html(`<canvas id="kChart"></canvas>`);
    var xNormalizadoChart = document.getElementById("xNormalizadoChart");
    var myLineChart = new Chart(xNormalizadoChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: datosgraficaLiquido,
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 10,
                    fontColor: 'black'
                }
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });


    var yNormalizadoChart = document.getElementById("yNormalizadoChart");
    var myLineChart = new Chart(yNormalizadoChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: datosgraficaVapor,
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 10,
                    fontColor: 'black'
                }
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });

    var temperaturaChart = document.getElementById("temperaturaChart");
    var myLineChart2 = new Chart(temperaturaChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: [{
                label: "Earnings",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: tj,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });


    var temperaturaChart = document.getElementById("ljChart");
    var myLineChart2 = new Chart(temperaturaChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: [{
                label: "Earnings",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: lj2,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });




    var vjChart = document.getElementById("vjChart");
    var myLineChart5 = new Chart(vjChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: [{
                label: "Earnings",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: vj2,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });


    var pjChart = document.getElementById("pjChart");
    var myLineChart5 = new Chart(pjChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: [{
                label: "Earnings",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: pj,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });

    var kChart = document.getElementById("kChart");
    var myLineChart3 = new Chart(kChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: datosgraficaKij,
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 10,
                    fontColor: 'black'
                }
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });
}
